package com.jellware.marsrover;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.jellware.marsrover.model.DataManager;
import com.jellware.marsrover.model.Orientation;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    //Screen views
    private LinearLayout layScenario, layStart, layBoundaries, layStartRover, layInstructions;
    private TextView txtTopBoundary, txtRightBoundary, txtStartRover, txtInstruction;
    private Button btnStart, btnContinue, btnStartRover, btnSend;

    //Grid views
    private LinearLayout layMarsBoundaries, layRoverLocation;
    private TextView txtMarsBoundaries, txtRoverCoordinates, txtRoverOrientation, txtRoverLost;
    private ImageView imgNorth, imgSouth, imgWest, imgEast;
    private Button btnReset;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initViews();

        //Init process
        layScenario.setVisibility(View.VISIBLE);
        layStart.setVisibility(View.VISIBLE);
        layBoundaries.setVisibility(View.GONE);
        layStartRover.setVisibility(View.GONE);
        layInstructions.setVisibility(View.GONE);

    }

    @Override
    public void onClick(View v) {
        if (v.equals(btnStart)) {
            layStart.setVisibility(View.GONE);
            layBoundaries.setVisibility(View.VISIBLE);
            layStartRover.setVisibility(View.GONE);
            layInstructions.setVisibility(View.GONE);
        } else if (v.equals(btnContinue)) {

            String topBoundary = txtTopBoundary.getText().toString();
            String rightBoundary = txtRightBoundary.getText().toString();

            if (topBoundary.length() > 0 && rightBoundary.length() > 0) {
                int marsTopBoundary = Integer.parseInt(topBoundary);
                int marsRightBoundary = Integer.parseInt(rightBoundary);

                if (marsRightBoundary > 50 || marsTopBoundary > 50) {
                    Toast.makeText(this, "Mars cannot be that big!", Toast.LENGTH_SHORT).show();

                } else {
                    DataManager.getInstance().initMars(marsTopBoundary, marsRightBoundary);

                    //Update UI
                    layMarsBoundaries.setVisibility(View.VISIBLE);
                    txtMarsBoundaries.setText(new StringBuilder().append(DataManager.getInstance().getMars().getRightBoundary()).append("x").append(DataManager.getInstance().getMars().getTopBoundary()).toString());

                    layStart.setVisibility(View.GONE);
                    layBoundaries.setVisibility(View.GONE);
                    layStartRover.setVisibility(View.VISIBLE);
                    layInstructions.setVisibility(View.GONE);
                }
            } else {
                Toast.makeText(this, "Mars boundaries cannot be empty", Toast.LENGTH_SHORT).show();
            }
        } else if (v.equals(btnStartRover)) {
            String instruction = txtStartRover.getText().toString();
            boolean error = DataManager.getInstance().initRover(instruction);

            if (error)
                Toast.makeText(this, "Unrecognized init order", Toast.LENGTH_SHORT).show();
            else {
                //Update UI
                layRoverLocation.setVisibility(View.VISIBLE);
                txtRoverCoordinates.setText(getString(R.string.coordinates, DataManager.getInstance().getMarsRover().getPosition().getX() + "," + DataManager.getInstance().getMarsRover().getPosition().getY()));
                txtRoverOrientation.setText(getString(R.string.orientation, DataManager.getInstance().getMarsRover().getOrientation().toString()));

                //Update orientation in UI
                selectCurrentOrientation(DataManager.getInstance().getMarsRover().getOrientation());
                layStart.setVisibility(View.GONE);
                layBoundaries.setVisibility(View.GONE);
                layStartRover.setVisibility(View.GONE);
                layInstructions.setVisibility(View.VISIBLE);
                btnReset.setVisibility(View.VISIBLE);
            }

        } else if (v.equals(btnSend)) {

            String instruction = txtInstruction.getText().toString();
            boolean lost = DataManager.getInstance().sendInstructionToRover(instruction);

            //Update UI
            txtRoverCoordinates.setText(getString(R.string.coordinates, DataManager.getInstance().getMarsRover().getPosition().getX() + "," + DataManager.getInstance().getMarsRover().getPosition().getY()));
            txtRoverOrientation.setText(getString(R.string.orientation, DataManager.getInstance().getMarsRover().getOrientation().toString()));

            //Update orientation in UI
            selectCurrentOrientation(DataManager.getInstance().getMarsRover().getOrientation());

            if (lost) {
                txtRoverLost.setVisibility(View.VISIBLE);
            }

        } else if (v.equals(btnReset)) {

            //Reset the rover
            DataManager.getInstance().resetMission();

            //Reset UI
            layScenario.setVisibility(View.VISIBLE);
            layStart.setVisibility(View.VISIBLE);
            layBoundaries.setVisibility(View.GONE);
            layStartRover.setVisibility(View.GONE);
            layInstructions.setVisibility(View.GONE);
            layMarsBoundaries.setVisibility(View.GONE);
            layRoverLocation.setVisibility(View.GONE);
            btnReset.setVisibility(View.GONE);
            txtRoverLost.setVisibility(View.GONE);

            txtRightBoundary.setText("");
            txtTopBoundary.setText("");
            txtStartRover.setText("");
            txtInstruction.setText("");
            selectCurrentOrientation(null);

        }
    }


    private void initViews() {
        //Init views
        layScenario = findViewById(R.id.layScenario);
        layStart = findViewById(R.id.layStart);
        layBoundaries = findViewById(R.id.layBoundaries);
        layInstructions = findViewById(R.id.layInstructions);
        layStartRover = findViewById(R.id.layRoverPosition);

        txtTopBoundary = findViewById(R.id.txtTopBoundary);
        txtRightBoundary = findViewById(R.id.txtRightBoundary);
        txtInstruction = findViewById(R.id.txtInstructions);
        txtStartRover = findViewById(R.id.txtRoverPosition);

        //Init grid views
        layMarsBoundaries = findViewById(R.id.layMarsBoundaries);
        layRoverLocation = findViewById(R.id.layRoverCoordinates);

        txtMarsBoundaries = findViewById(R.id.txtMarsBoundaries);
        txtRoverCoordinates = findViewById(R.id.txtRoverCoordinates);
        txtRoverOrientation = findViewById(R.id.txtRoverOrientation);
        txtRoverLost = findViewById(R.id.txtRoverLost);

        imgEast = findViewById(R.id.imgEast);
        imgNorth = findViewById(R.id.imgNorth);
        imgWest = findViewById(R.id.imgWest);
        imgSouth = findViewById(R.id.imgSouth);

        btnStart = findViewById(R.id.btnStart);
        btnStart.setOnClickListener(this);
        btnContinue = findViewById(R.id.btnContinue);
        btnContinue.setOnClickListener(this);
        btnSend = findViewById(R.id.btnSend);
        btnSend.setOnClickListener(this);
        btnStartRover = findViewById(R.id.btnStartRover);
        btnStartRover.setOnClickListener(this);
        btnReset = findViewById(R.id.btnReset);
        btnReset.setOnClickListener(this);

        layMarsBoundaries.setVisibility(View.GONE);
        layRoverLocation.setVisibility(View.GONE);
        btnReset.setVisibility(View.GONE);

    }

    private void selectCurrentOrientation(Orientation roverOrientation) {

        if (roverOrientation == null) {
            imgNorth.setVisibility(View.VISIBLE);
            imgSouth.setVisibility(View.VISIBLE);
            imgEast.setVisibility(View.VISIBLE);
            imgWest.setVisibility(View.VISIBLE);
        } else {
            switch (roverOrientation) {
                case NORTH:
                    imgNorth.setVisibility(View.VISIBLE);
                    imgSouth.setVisibility(View.GONE);
                    imgEast.setVisibility(View.GONE);
                    imgWest.setVisibility(View.GONE);
                    break;
                case SOUTH:
                    imgNorth.setVisibility(View.GONE);
                    imgSouth.setVisibility(View.VISIBLE);
                    imgEast.setVisibility(View.GONE);
                    imgWest.setVisibility(View.GONE);
                    break;
                case EAST:
                    imgNorth.setVisibility(View.GONE);
                    imgSouth.setVisibility(View.GONE);
                    imgEast.setVisibility(View.VISIBLE);
                    imgWest.setVisibility(View.GONE);
                    break;
                case WEST:
                    imgNorth.setVisibility(View.GONE);
                    imgSouth.setVisibility(View.GONE);
                    imgEast.setVisibility(View.GONE);
                    imgWest.setVisibility(View.VISIBLE);
                    break;
            }
        }
    }
}
