package com.jellware.marsrover.model;


import android.util.Log;

public class Rover {

    private Orientation orientation;
    private Coordinate position;

    public Rover(Orientation orientation, Coordinate position) {
        this.orientation = orientation;
        this.position = position;
    }

    /**
     * Turn the Rover to the right
     */
    public void turnRight() {
        switch (orientation) {
            case NORTH:
                orientation = Orientation.EAST;
                break;
            case EAST:
                orientation = Orientation.SOUTH;
                break;
            case SOUTH:
                orientation = Orientation.WEST;
                break;
            case WEST:
                orientation = Orientation.NORTH;
                break;
        }
    }

    /**
     * Turn the rover to the left
     */
    public void turnLeft() {
        switch (orientation) {
            case NORTH:
                orientation = Orientation.WEST;
                break;
            case EAST:
                orientation = Orientation.NORTH;
                break;
            case SOUTH:
                orientation = Orientation.EAST;
                break;
            case WEST:
                orientation = Orientation.SOUTH;
                break;
        }
    }

    /**
     * Move to the next position, based on current position and orientation
     */
    public void forward() {
        switch (orientation) {
            case NORTH:
                position.setY(position.getY() + 1);
                break;
            case EAST:
                position.setX(position.getX() + 1);
                break;
            case SOUTH:
                position.setY(position.getY() - 1);
                break;
            case WEST:
                position.setX(position.getX() - 1);
                break;
        }
    }

    /**
     * Place the Mars Rover in a coordinate within Mars
     * @param initInstruction
     * @param mars
     * @return true if error
     */
    public boolean landRoverInMars(String initInstruction, Mars mars) {
        String[] data = initInstruction.split(" ");

        if (data.length != 3)
            return true;
        String rawX = data[0];
        String rawY = data[1];
        String rawOrientation = data[2];

        int xPos, yPos;
        Orientation orientation;
        try {
            xPos = Integer.parseInt(rawX);
        } catch (Exception e) {
            return true;
        }

        try {
            yPos = Integer.parseInt(rawY);
        } catch (Exception e) {
            return true;
        }

        if(xPos < 0 || yPos < 0 || xPos > mars.getRightBoundary() || yPos > mars.getTopBoundary())
            return true;

        switch (rawOrientation) {
            case "N":
                orientation = Orientation.NORTH;
                break;
            case "S":
                orientation = Orientation.SOUTH;
                break;
            case "E":
                orientation = Orientation.EAST;
                break;
            case "W":
                orientation = Orientation.WEST;
                break;
            default:
                return true;
        }

        setOrientation(orientation);
        setPosition(new Coordinate(xPos, yPos));
        return false;
    }

    /**
     * Execute an instruction in Mars
     * @param instruction
     * @param planetMars
     * @return true if error
     */
    public boolean executeInstruction(String instruction, Mars planetMars) {
        char[] orders = instruction.toCharArray();
        boolean lost = false;
        for (char order : orders) {
            Coordinate lastKnownPosition = new Coordinate(position.getX(), position.getY());
            if (order == 'R')
                turnRight();
            else if (order == 'L')
                turnLeft();
            else if (order == 'F') {
                forward();
            }else {
                //TODO We can add more command orders here in the future
                Log.e("Rover", "Order not recognized");
            }

            if (position.getY() < 0 || position.getX() < 0 || position.getX() > planetMars.getRightBoundary() || position.getY() > planetMars.getTopBoundary()) {
                position = lastKnownPosition;
                lost = true;
                break;
            }

        }
        return lost;
    }




    public Orientation getOrientation() {
        return orientation;
    }

    public void setOrientation(Orientation orientation) {
        this.orientation = orientation;
    }

    public Coordinate getPosition() {
        return position;
    }

    public void setPosition(Coordinate position) {
        this.position = position;
    }
}
