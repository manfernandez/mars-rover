package com.jellware.marsrover.model;


/**
 * Singleton class to handle the Mars Rover and Mars Planet.
 * Has the relationship between Mars and the Rover
 */
public class DataManager {

    private static DataManager singleton;
    private Rover marsRover;
    private Mars mars;


    public static DataManager getInstance(){
        if(singleton == null)
            singleton = new DataManager();
        return singleton;
    }

    private DataManager(){
        marsRover = new Rover(Orientation.NORTH, new Coordinate(0,0));
        mars = new Mars(0,0);
    }

    /**
     * Init the Mars boundaries
     * @param topBoundary
     * @param rightBoundary
     */
    public void initMars(int topBoundary, int rightBoundary){
        mars.setRightBoundary(rightBoundary);
        mars.setTopBoundary(topBoundary);
    }

    /**
     * Init the position of the Rover in Mars
     * @param startInstruction
     * @return true if error
     */
    public boolean initRover(String startInstruction){
        return marsRover.landRoverInMars(startInstruction, mars);
    }

    /**
     * Send a instruction to the Rover.
     * @param instruction
     * @return true if error
     */
    public boolean sendInstructionToRover(String instruction){
        return marsRover.executeInstruction(instruction, mars);
    }

    /**
     * Resets the mission
     */
    public void resetMission() {
        singleton = null;
    }

    public Rover getMarsRover() {
        return marsRover;
    }

    public Mars getMars() {
        return mars;
    }
}
