package com.jellware.marsrover.model;


public enum Orientation {
    NORTH, EAST, SOUTH, WEST
}
