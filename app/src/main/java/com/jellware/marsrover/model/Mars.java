package com.jellware.marsrover.model;

/**
 * Created by manuelfernandez on 08/04/2019.
 */

public class Mars {

    private int topBoundary;
    private int rightBoundary;

    public Mars(int topBoundary, int rightBoundary) {
        this.topBoundary = topBoundary;
        this.rightBoundary = rightBoundary;
    }

    public int getTopBoundary() {
        return topBoundary;
    }

    public void setTopBoundary(int topBoundary) {
        this.topBoundary = topBoundary;
    }

    public int getRightBoundary() {
        return rightBoundary;
    }

    public void setRightBoundary(int rightBoundary) {
        this.rightBoundary = rightBoundary;
    }
}
