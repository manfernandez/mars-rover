package com.jellware.marsrover;

import android.content.Intent;


import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.clearText;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {

    @Rule
    public ActivityTestRule<MainActivity> activityRule = new ActivityTestRule<>(
            MainActivity.class,
            true,     // initialTouchMode
            false);   // launchActivity. False so we can customize the intent per test method

    @Test
    public void launchRoverWithSuccessInstructions() throws Exception {


        Intent intent = new Intent(getInstrumentation().getTargetContext(), MainActivity.class);
        activityRule.launchActivity(intent);
        Thread.sleep(1000);
        onView(withId(R.id.btnStart)).perform(click());

        onView(withId(R.id.txtRightBoundary)).perform(clearText(), replaceText("5"), closeSoftKeyboard());
        onView(withId(R.id.txtTopBoundary)).perform(clearText(), replaceText("3"), closeSoftKeyboard());
        onView(withId(R.id.btnContinue)).perform(click());
        onView(withId(R.id.txtRoverPosition)).perform(clearText(), replaceText("1 1 E"), closeSoftKeyboard());
        onView(withId(R.id.btnStartRover)).perform(click());
        onView(withId(R.id.txtInstructions)).perform(clearText(), replaceText("RFRFRFRF"), closeSoftKeyboard());
        onView(withId(R.id.btnSend)).perform(click());

        onView(withId(R.id.txtRoverCoordinates)).check(matches(withText("Coordinates: (1,1)")));
        onView(withId(R.id.txtRoverOrientation)).check(matches(withText("Orientation: (EAST)")));

    }

    @Test
    public void launchRoverWithLostInstructions() throws Exception {


        Intent intent = new Intent(getInstrumentation().getTargetContext(), MainActivity.class);
        activityRule.launchActivity(intent);
        Thread.sleep(1000);
        onView(withId(R.id.btnStart)).perform(click());

        onView(withId(R.id.txtRightBoundary)).perform(clearText(), replaceText("5"), closeSoftKeyboard());
        onView(withId(R.id.txtTopBoundary)).perform(clearText(), replaceText("3"), closeSoftKeyboard());
        onView(withId(R.id.btnContinue)).perform(click());
        onView(withId(R.id.txtRoverPosition)).perform(clearText(), replaceText("3 2 N"), closeSoftKeyboard());
        onView(withId(R.id.btnStartRover)).perform(click());
        onView(withId(R.id.txtInstructions)).perform(clearText(), replaceText("FRRFLLFFRRFLL"), closeSoftKeyboard());
        onView(withId(R.id.btnSend)).perform(click());

        onView(withId(R.id.txtRoverCoordinates)).check(matches(withText("Coordinates: (3,3)")));
        onView(withId(R.id.txtRoverOrientation)).check(matches(withText("Orientation: (NORTH)")));
        onView(withId(R.id.txtRoverLost)).check(matches(isDisplayed()));

    }
}
